#![forbid(unsafe_code)]

use crate::cli_handler::generate_print_save;
use heck::{CamelCase, KebabCase};
use itertools::Itertools;
use once_cell::sync::Lazy;
use std::{error::Error, path::PathBuf};
use structopt::StructOpt;
use strum::IntoEnumIterator;
use tpfs_krypt::XandKeyType;

mod cli_handler;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "xkeygen",
    about = "Xand network utility tools to generate or transform keys related to validators, p2p, \
             and member wallets"
)]
pub(crate) enum XandKeygenCli {
    #[structopt(
        name = "generate",
        about = "Generate random keypairs and save keyfile in the \
    undocumented and odd formats downstream systems expect to consume them (substrate)"
    )]
    Generate {
        /// Type of key to generate
        #[structopt(parse(try_from_str = parse_key_type), help = &**KEY_TYPE_HELP)]
        key_type: XandKeyType,

        /// Directory in which to write keyfile
        #[structopt(long = "out-dir", parse(from_os_str), default_value = ".")]
        out_dir: PathBuf,
    },
}

static KEY_TYPE_HELP: Lazy<String> = Lazy::new(|| {
    format!(
        "Type of key to generate. One of:\n{}",
        XandKeyType::iter()
            .map(|t| format!("- {}", t.to_string().to_kebab_case()))
            .format("\n"),
    )
});

fn parse_key_type(s: &str) -> Result<XandKeyType, Box<dyn Error>> {
    Ok(s.to_camel_case().parse()?)
}

fn main() {
    let opt = XandKeygenCli::from_args();

    match opt {
        XandKeygenCli::Generate { key_type, out_dir } => {
            generate_print_save(key_type, out_dir).unwrap();
        }
    }
}
