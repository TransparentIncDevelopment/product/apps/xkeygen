<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Xand Keygen](#xand-keygen)
    - [Usage](#usage)
  - [Under the hood](#under-the-hood)
      - [Why are the validadtor keys and libp2p keys different?](#why-are-the-validadtor-keys-and-libp2p-keys-different)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Xand Keygen

This is a CLI tool for generating and/or validating keypairs related to different components 
in the Xand platform. The tool serializes the keypairs into the file formats Substrate expects to read in,
and also will generate the appropriate base58 encoding (address) of the underlying keypair for use 
in UIs, configurations, etc. 

The following key types are currently supported:

1. Validator - key used by validator node to produce and sign blocks
1. LibP2P - key used by validator node for secure 'gossip' (communication) between nodes
1. Wallet - key type used to issue transactions on the blockchain (member user, wallet user, OpCo transactions)
1. Shared encryption - key type used for confidentiality on the network (encrypting/decrypting on-chain data)
1. Trust - key used to authorize creations and redemptions.

Regardless of the underlying key type (secp256k1, ed25519, some futuristic-quantum-safe magic keytype),
this tool would generate the appropriate keypair for the target component such that it is safely (de)serializable 
back and forth from file. 

### Usage

There are two main modes, generate and load. For usage, see:
 
```sh
cargo run --bin xkeygen -- --help
```

## Under the hood
#### Why are the validator keys and libp2p keys different? 
A validator *node* requires a "Validator" key ("AuthorityKey" in Substrate) for signing blocks, and a libP2P key ("node key" in Substrate)
for secured p2p communications.
We have chosen to use ed25519 for both keytypes. The mapping from each key to address is slightly different, because
of their different contexts.

To get the address for a libp2p key, we use the libp2p libraries to derive the address given the public key. 
Under the hood, it converts the public key into a "multihash" ([source](https://github.com/libp2p/rust-libp2p/blob/c0b379b908a2f1f622cd205c6890a520bc8f5949/core/src/peer_id.rs#L55)) 
before base58 encoding.
The resulting libp2p address is also referred to as a "PeerId".

The validator key base58 address takes into account a "chain identifier" ([source](https://github.com/paritytech/substrate/blob/fc206f3a009b64fc746202e5b4c701bf7e24d1f1/core/primitives/src/crypto.rs#L258-L258)).
 In the link to source, you can see that it defaults to `42` in the commit of Substrate we are using. It looks like the Substrate docs for "chain identifier" have changed since 
 we first looked into it, but our Pivotal story with notes is here: https://www.pivotaltracker.com/story/show/166780977

## Resources
[Domain Types and Encoding/Serialization](https://transparentinc.atlassian.net/wiki/spaces/PROD/pages/1306624009/Domain+Types+and+Canonical+String+Encoding+Serialization)
[Key Types](https://transparentinc.atlassian.net/wiki/spaces/PROD/pages/1327300609/Key+Types+and+Origins)
